import FaqAPI from './faq-api.js';
import DOMUtilities from '../../utilities/dom.js';

class FAQ {
  constructor() {
    this.state = {
      data: [],
      fetchDataRequestStatus: undefined,
      inputValue: '',
    };

    this.searchTimeout = undefined;
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleFaqItemClick = this.handleFaqItemClick.bind(this);

    this.elements = {
      root: document.querySelector('.faq'),
    };

    this.render();
    this.loadData();
  }

  // DOM
  // =============================================
  addElements(elements) {
    const newElements = {};

    Object.keys(elements).forEach(elementKey => {
      const elementItem = elements[elementKey];

      newElements[elementKey] = elementItem.nodes;
    });

    // No object spread?
    this.elements = Object.assign(this.elements, newElements);
  }

  // Async
  // =============================================
  loadData(value) {
    const success = data => {
      this.setState({
        data,
        fetchDataRequestStatus: 'success',
      });

      this.buildUI('success');
    };

    const fail = () => {
      this.setState({
        data: [],
        fetchDataRequestStatus: 'fail',
      });

      this.buildUI('fail');
    };

    // Pending
    this.setState({
      fetchDataRequestStatus: 'pending',
    });

    this.buildUI('pending');

    // Request data
    FaqAPI.fetchData(value)
      .then(success)
      .catch(fail)
    ;
  }

  // Utils
  // =============================================
  setState(newState) {

    // No object spread?
    this.state = Object.assign(this.state, newState);
  }

  // Event Handlers
  // =============================================
  handleInputChange(event) {
    const { value } = event.target;

    window.clearTimeout(this.searchTimeout);

    this.searchTimeout = window.setTimeout(() => {
      this.state.inputValue = value;

      this.loadData({
        question: value,
      });
    }, 400);
  }

  handleFaqItemClick(event) {
    const closestElement = event.target.closest('.faq-item');

    const elementClasses = closestElement.getAttribute('class').split(' ');
    const newClasses = [...elementClasses];
    const activeClassIndex = newClasses.indexOf('faq-item--open');

    if (activeClassIndex !== -1) {
      newClasses.splice(activeClassIndex, 1);
    } else {
      newClasses.push('faq-item--open');
    }

    closestElement.setAttribute('class', newClasses.join(' '));
  }

  // Builders
  // =============================================
  buildSuccess() {
    const { data } = this.state;

    if (!data.length) {
      return '<p>no items available</p>';
    }

    return `
      ${data.map(item => `
        <div class='faq-item'>
          <div class='faq-item__header'>
            <div class='faq-item__clickable-area'>
              <button>${item.question}</button>
            </div>

            <div class='faq-item__text'>
              <span>${item.question}</span>
            </div>
            <div class='faq-item__icon'>
              <i></i>
            </div>
          </div>
          <div class='faq-item__body'>
            <div class='faq-item__body-container'>
              <p>${item.answer}</p>
            </div>
          </div>
        </div>
      `).join('')}
    `;
  }

  buildFail() {
    return '<p>Oops, I\'ve emulated a rejected promise. This is expected and random. See faq-api.js. just refresh to make it work.</p>';
  }

  buildPending() {
    return '<p>Fetching items</p>';
  }

  buildUI(type) {
    let markup;

    switch(type) {
      case 'success': markup = this.buildSuccess(); break;
      case 'fail': markup = this.buildFail(); break;
      case 'pending': markup = this.buildPending(); break;
    }

    this.elements.items.innerHTML = markup;

    const element = {
      buttons: {
        nodes: document.querySelectorAll(`.faq .faq-item button`),
        events: {
          click: this.handleFaqItemClick,
        },
      }
    };

    DOMUtilities.toggleEvents('add', element);

    this.addElements(element);
  }

  // Render
  // =============================================
  render() {
    this.elements.root.innerHTML = `
      <div class='faq-input'>
        <input type='text' value='' placeholder='search FAQ' name='faq-search' />
      </div>
      <div class='faq-items'>&nbsp;</div>
    `;

    const elements = {
      input: {
        nodes: document.querySelector(`.faq .faq-input input`),
        events: {
          input: this.handleInputChange,
        },
      },
      items: {
        nodes: document.querySelector(`.faq .faq-items`),
      }
    };

    DOMUtilities.toggleEvents('add', elements);

    this.addElements(elements);
  }
}

export default FAQ;
