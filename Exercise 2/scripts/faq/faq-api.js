import mockData from './__mocks__/faq.json';
import ObjectUtilities from '../../utilities/object.js';

// I am mocking a "fetch" using a Promise.
const fetchData = payload => {

  // we emulate a rejected promise
  // if 5 then reject.
  const shouldReject = (parseInt(Math.random() * 10, 10) + 1) === 5;

  return new Promise((resolve, reject) => {
    return window.setTimeout(() => {

      if (shouldReject) {
        return reject('error');
      }

      let newData = mockData;

      if (ObjectUtilities.isObject(payload)) {
        if (payload.question) {
          newData = mockData.filter((item) => {
            const itemValue = item.question.toLowerCase().trim();
            const newValue = payload.question.toLowerCase().trim();

            return itemValue.indexOf(newValue) !== -1;
          });
        }
      }

      resolve(newData);
    }, Math.random() * 1000);
  });
};

export default {
  fetchData,
};
