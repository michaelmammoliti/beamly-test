const gulp = require('gulp');
const tasks = require('gulp-standard-tasks')(gulp);

//
// Sass tasks
//

gulp.task('sass-dev', tasks.css({
	src: 'scss/*.scss',
	dest: 'css',
	browserSync: true,
    customSassOptions: {
        includePaths: [
            './node_modules/beamly-base-css/src/scss'
        ]
    }
}));

//
// JavaScript tasks
//

gulp.task('browserify', tasks.browserify({
    src: 'scripts/docs.js',
    dest: 'scripts',
    bundleName: 'app-bundle.min.js'
}));

gulp.task('watchify', tasks.browserify({
    src: 'scripts/app.js',
    dest: 'scripts',
    bundleName: 'app-bundle.min.js',
    watch: true
}));

//
// Reload on index.html changes
//

gulp.task('serve', ['sass-dev'], function() {
    tasks.browserSync({
        server: {
            baseDir: './'
        }
    });
});


//
// Build the documentation using Jekyll
//

gulp.task('reload', () => tasks.browserSync.reload());

//
// Watch assets for changes and rebuild the files,
//

gulp.task('watch', ['watchify'], function () {
    gulp.watch('scss/**/*.scss', ['sass-dev']);
    gulp.watch('scripts/**/*.js', ['reload']);
    gulp.watch(['index.html'], ['reload']);
    gulp.watch(['faq.html'], ['reload']);
});

//
// Define a default task that sets up the server and watches files
//
gulp.task('default', ['serve', 'watch']);
