const isObject = (payload) => {
  return !!(payload && !Array.isArray(payload) && typeof payload === 'object');
};

export default {
  isObject,
};
