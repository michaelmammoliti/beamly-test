import ObjectUtilities from './object.js';

const addEvent = (method, element, event) => {
  const methodName = `${method}EventListener`;

  if (
    !element ||
    !event.name ||
    !event.handler ||
    ['remove', 'add'].indexOf(methodName) !== -1
  ) {
    return;
  }

  element[methodName](event.name, event.handler);
};

const toggleEvents = (method, elements) => {
  Object.keys(elements).forEach(elementKey => {
    const elementItem = elements[elementKey];

    const elementNodes = ('length' in elementItem.nodes)
      ? elementItem.nodes
      : [elementItem.nodes]
    ;

    if (!ObjectUtilities.isObject(elementItem.events)) {
      return;
    }

    elementNodes.forEach(nodeItem => {
      Object.keys(elementItem.events).forEach(eventKey => {
        const eventItem = {
          name: eventKey,
          handler: elementItem.events[eventKey],
        };

        addEvent(method, nodeItem, eventItem);
      });
    });
  });
};

export default {
  addEvent,
  toggleEvents,
};
