# Exercise 1 (Product Card)

For the first exercise we would like you to build a product card for us, the design is below:

## Getting started
To get you started, you are provided with a base template which includes the following:
- Base HTML
- A JSON file containing the FAQs
- Sass including some basic styles to get your started
- Babel
- Browserify
- Browsersync

To set this up on your machine you should be able to simply use ‘npm install’ in the the directory of the exercise. To start the exercise please run ‘npx gulp’ (this assumes you have a relatively recent version of Node and NPM).

## Notes:
- The card should be responsive with the max width of the product card being 450px. The smallest device you will need to support is 320px.
- The ‘New’ badge should be live text rather than embedding as an image
- Use BEM methodology for class names
- Font used it Lato from Google Fonts
- Font sizes are: Title: 22px, Description 14px, Price: 14px
